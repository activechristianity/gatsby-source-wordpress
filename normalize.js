"use strict";

var readingTime = require('reading-time');

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

const crypto = require(`crypto`);

const deepMapKeys = require(`deep-map-keys`);

const _ = require(`lodash`);

const _require = require(`gatsby-source-filesystem`),
      createRemoteFileNode = _require.createRemoteFileNode;

const colorized = require(`./output-color`);

const conflictFieldPrefix = `wordpress_`; // restrictedNodeFields from here https://www.gatsbyjs.org/docs/node-interface/

const restrictedNodeFields = [`id`, `children`, `parent`, `fields`, `internal`];

global.hasWPTypes = {
  'wordpress__POST': false,
  'wordpress__PAGE': false,
  'wordpress__wp_glossary': false,
  'wordpress__wp_ac_essential': false,
  'wordpress__wp_ac_media': false,
  'wordpress__wp_ac_author': false,
  'wordpress__wp_ac_ebook': false,
  'wordpress__wp_ac_banner': false,
  'wordpress__TAG': false,
  'wordpress__CATEGORY': false,
}

/**
 * Encrypts a String using md5 hash of hexadecimal digest.
 *
 * @param {any} str
 */
const digest = str => crypto.createHash(`md5`).update(str).digest(`hex`);

/**
 * Validate the GraphQL naming convetions & protect specific fields.
 *
 * @param {any} key
 * @returns the valid name
 */
function getValidKey({
  key,
  verbose = false
}) {
  let nkey = String(key);
  const NAME_RX = /^[_a-zA-Z][_a-zA-Z0-9]*$/;
  let changed = false; // Replace invalid characters

  if (!NAME_RX.test(nkey)) {
    changed = true;
    nkey = nkey.replace(/-|__|:|\.|\s/g, `_`);
  } // Prefix if first character isn't a letter.


  if (!NAME_RX.test(nkey.slice(0, 1))) {
    changed = true;
    nkey = `${conflictFieldPrefix}${nkey}`;
  }

  if (restrictedNodeFields.includes(nkey)) {
    changed = true;
    nkey = `${conflictFieldPrefix}${nkey}`.replace(/-|__|:|\.|\s/g, `_`);
  }

  if (changed && verbose) console.log(colorized.out(`Object with key "${key}" breaks GraphQL naming convention. Renamed to "${nkey}"`, colorized.color.Font.FgRed));
  return nkey;
}

exports.getValidKey = getValidKey; // Remove the ACF key from the response when it's not an object

const normalizeACF = entities => entities.map(e => {
  if (!_.isPlainObject(e[`acf`])) {
    delete e[`acf`];
  }

  return e;
});

exports.normalizeACF = normalizeACF; // Combine all ACF Option page data

exports.combineACF = function (entities) {
  let acfOptionData = {}; // Map each ACF Options object keys/data to single object

  const acf_options = entities.filter(e => e.__type === `wordpress__acf_options`)

  acf_options.forEach(e => {
    if (e.acf) {
      const tmpData = acfOptionData[e.__acfOptionPageId || `options`] = {};
      Object.keys(e.acf).map(k => tmpData[k] = e.acf[k]);
    }
  }); // Remove previous ACF Options objects (if any)

  _.pullAll(entities, acf_options); // Create single ACF Options object

  entities.push({
    acf: acfOptionData || false,
    __type: `wordpress__acf_options`
  });
  return entities;
}; // Create entities from the few the WordPress API returns as an object for presumably
// legacy reasons.


const normalizeEntities = entities => {
  const mapType = e => Object.keys(e).filter(key => key !== `__type`).map((key) => {
    return Object.assign({
      id: key
    }, e[key], {
      __type: e.__type
    });
  });

  return entities.reduce((acc, e) => {
    switch (e.__type) {
      case `wordpress__wp_types`:
        return acc.concat(mapType(e));

      case `wordpress__wp_api_menus_menu_locations`:
        return acc.concat(mapType(e));

      case `wordpress__wp_statuses`:
        return acc.concat(mapType(e));

      case `wordpress__wp_taxonomies`:
        return acc.concat(mapType(e));

      case `wordpress__acf_options`:
        return acc.concat(mapType(e));

      case `wordpress__wp_options`:
        return acc.concat(mapType({ options: e, __type: 'wordpress__wp_options' }));

      default:
        return acc.concat(e);
    }
  }, []);
};

exports.normalizeEntities = normalizeEntities; // Standardize ids + make sure keys are valid.

exports.standardizeKeys = entities => entities.map(e => deepMapKeys(e, key => key === `ID` ? getValidKey({
  key: `id`
}) : getValidKey({
  key
}))); // Standardize dates on ISO 8601 version.


exports.standardizeDates = entities => entities.map(e => {
  Object.keys(e).forEach((key) => {
    if (e[`${key}_gmt`]) {
      e[key] = new Date(e[`${key}_gmt`] + `z`).toJSON();
      delete e[`${key}_gmt`];
    }
  });
  return e;
}); // Lift "rendered" fields to top-level


exports.liftRenderedField = entities => entities.map(e => {
  Object.keys(e).forEach((key) => {
    const value = e[key];

    if (_.isObject(value) && _.isString(value.rendered)) {
      e[key] = value.rendered;
    }
  });
  return e;
}); // Exclude entities of unknown shape
// Assume all entities contain a wordpress_id,
// except for whitelisted type wp_settings and the site_metadata


exports.excludeUnknownEntities = entities => entities.filter(e => e.wordpress_id || e.__type === `wordpress__wp_settings` || e.__type === `wordpress__site_metadata`); // Excluding entities without ID, or WP Settings
// Create node ID from known entities
// excludeUnknownEntities whitelisted types don't contain a wordpress_id
// we create the node ID based upon type if the wordpress_id doesn't exist


const wpEntitiesRef = {}
exports.wpEntitiesRef = wpEntitiesRef

function findWpEntityRef(wpId, types, onFind, parRef) {
  types = typeof types === 'string' ? [types] : types

  if (!types.find(type => {
    if (wpEntitiesRef[type] && wpEntitiesRef[type][String(wpId)]) {
      onFind(wpEntitiesRef[type][String(wpId)])
      return true
    }
    return false
  })) console.error(`[${types.join(', ')}] #${wpId} not found for ${parRef}`)
}

exports.createGatsbyIds = (createNodeId, entities) => entities.map(e => {
  if (!wpEntitiesRef[e.__type]) wpEntitiesRef[e.__type] = {}

  if (e.wordpress_id) {
    e.id = createNodeId(`${e.__type}-${e.wordpress_id.toString()}`);
  } else {
    e.id = createNodeId(e.__type);
  }

  wpEntitiesRef[e.__type][String(e.wordpress_id)] = e.id
  global.hasWPTypes[e.__type] = true

  return e;
}); // Build foreign reference map.


exports.mapTypes = entities => {
  const groups = _.groupBy(entities, e => e.__type);

  for (let groupId in groups) {
    groups[groupId] = groups[groupId].map(e => {
      return {
        wordpress_id: e.wordpress_id,
        id: e.id
      };
    });
  }

  return groups;
};

exports.mapAuthorsToUsers = entities => {
  const users = entities.filter(e => e.__type === `wordpress__wp_users`);
  return entities.map(e => {
    if (users.length && e.author) {
      // Find the user
      const user = users.find(u => u.wordpress_id === e.author);

      if (user) {
        e.author___NODE = user.id; // Add a link to the user to the entity.

        if (!user.all_authored_entities___NODE) {
          user.all_authored_entities___NODE = [];
        }

        user.all_authored_entities___NODE.push(e.id);

        if (!user[`authored_${e.__type}___NODE`]) {
          user[`authored_${e.__type}___NODE`] = [];
        }

        user[`authored_${e.__type}___NODE`].push(e.id);
        delete e.author;
      }
    }

    return e;
  });
};

exports.mapCustomAcFields = entities => {
  const ac_authors = wpEntitiesRef['wordpress__wp_ac_author']

  return entities.map(e => {
    if (e.__type === 'wordpress__wp_ac_media' || e.__type ==='wordpress__POST') {
      

      if (e.acf && e.acf.ac_read_more_posts){
        const read_more_posts={}
        read_more_posts.posts=[]
        if (typeof e.acf.ac_read_more_posts ==="object"){
          for (const key in e.acf.ac_read_more_posts){
            if (key==="main_text"){
              read_more_posts.main_text = e.acf.ac_read_more_posts[key]
            }
            if (key.startsWith("link_")){
              if (typeof e.acf.ac_read_more_posts[key]==="string"){
                read_more_posts.posts.push(e.acf.ac_read_more_posts[key])
              }
              
              
            }
          }
          e.read_more_posts=read_more_posts

          delete e.acf.ac_read_more_posts

        }
      }
    }
    if (e.__type === 'wordpress__wp_ac_media') {

      e.ac_lyrics___NODE = []
      if (e.acf && Array.isArray(e.acf._ac_media_lyrics)) {
        e.acf._ac_media_lyrics.forEach(aId => {
          findWpEntityRef(aId, ['wordpress__wp_ac_author'], (ref) => {
            e.ac_lyrics___NODE.push(ref)
          }, `${e.type}_${e.wordpress_id}`)
        })
        delete e.acf._ac_media_lyrics
      }

      e.ac_vocals___NODE = []
      if (e.acf && Array.isArray(e.acf._ac_media_vocals)) {
        e.acf._ac_media_vocals.forEach(aId => {
          findWpEntityRef(aId, ['wordpress__wp_ac_author'], (ref) => {
            e.ac_vocals___NODE.push(ref)
          }, `${e.type}_${e.wordpress_id}`)
        })
        delete e.acf._ac_media_vocals
      }

      e.ac_media_categories___NODE = []
      if (Array.isArray(e.ac_media_category)) {
        e.ac_media_category.forEach(cId => {
          findWpEntityRef(cId, ['wordpress__wp_ac_media_category'], (ref) => {
            e.ac_media_categories___NODE.push(ref)
          }, `${e.type}_${e.wordpress_id}`)
        })
        delete e.ac_media_category
      }

      if (e.acf && e.acf.ac_itunes_podcast_info){
        const itunes_podcast_info={}
        if (typeof e.acf.ac_itunes_podcast_info ==="object"){
          for (const key in e.acf.ac_itunes_podcast_info){
            if (key==="description" || key==="episode_notes"  || key==="image" ||key==="duration"){
              itunes_podcast_info[key] = e.acf.ac_itunes_podcast_info[key]
            }

          }
          e.itunes_podcast_info=itunes_podcast_info

          delete e.acf.ac_itunes_podcast_info

        }
      }

      if (e.acf) {
        e.ac_media_type = e.acf._ac_media_type
        e.ac_media_url = e.acf._ac_media_url

        delete e.acf._ac_media_type
        delete e.acf._ac_media_url
      }
    } else if (e.__type === 'wordpress__PAGE') {

      if (!e.acf.ac_template) e.acf.ac_template = ''

      if (Array.isArray(e.ac_children)) {
        e.ac_children___NODE = []
        e.ac_children.forEach(cId => {
          findWpEntityRef(cId, ['wordpress__PAGE'], (ref) => {
            e.ac_children___NODE.push(ref)
          }, `${e.type}_${e.wordpress_id}`)
          delete e.ac_children
        })
      }

      e.ac_parent___NODE = null
      if (e.parent) {
        findWpEntityRef(e.parent, ['wordpress__PAGE'], (ref => e.ac_parent___NODE = ref), `${e.type}_${e.wordpress_id}`)
        delete e.parent
      }

      if (Array.isArray(e.acf.topics)) {
        e.acf.topics.forEach(topic => {
          topic.tags___NODE = []
          topic.tags.forEach(tId => {
            findWpEntityRef(tId, ['wordpress__TAG'], (ref) => {
              topic.tags___NODE.push(ref)
            }, `${e.type}_${e.wordpress_id}`)
          })
          delete topic.tags
        })
      } else e.acf.topics = []

      e.acf.ac_filter_category___NODE = []
      if (Array.isArray(e.acf.ac_filter_category)) {
        e.acf.ac_filter_category.forEach(cId => {
          findWpEntityRef(cId, ['wordpress__CATEGORY'], (ref) => {
            e.acf.ac_filter_category___NODE.push(ref)
          }, `${e.type}_${e.wordpress_id}`)
        })
        delete e.acf.ac_filter_category
      } else e.acf.ac_filter_category = []

      e.acf.ac_filter_media_category___NODE = []
      if (e.acf.ac_filter_media_category) {
        e.acf.ac_filter_media_category___NODE = e.acf.ac_filter_media_category.map(mc => wpEntitiesRef['wordpress__wp_ac_media_category'][mc])
        delete e.acf.ac_filter_media_category
      } else e.acf.ac_filter_media_category = []

    } else if (e.__type === 'wordpress__wp_ac_ebook') {
      e.ac_ebook_related___NODE = []
      if (Array.isArray(e.acf._ac_ebook_related)) {
        e.acf._ac_ebook_related.forEach(wpObj => {
          findWpEntityRef(wpObj.wordpress_id, ['wordpress__wp_ac_ebook'], (ref) => {
            e.ac_ebook_related___NODE.push(ref)
          }, `${e.type}_${e.wordpress_id}`)
        })
        delete e.acf._ac_ebook_related
      }
    } else if (e.__type === 'wordpress__wp_ac_essential') {
      e.faq = []
      if (e.acf.faq) {
        e.faq = e.acf.faq
        delete e.acf.faq
      }
    }

    if (Array.isArray(e.yarpp)) {
      e.related_posts___NODE = []
      e.yarpp.forEach(pId => {
        findWpEntityRef(pId, ['wordpress__POST'], (ref => e.related_posts___NODE.push(ref)), `${e.type}_${e.wordpress_id}`)
      })
      delete e.acf.yarpp
    }

    if (e.acf) {
      if (e.acf._ac_ebook_banner) {
        e.ac_ebook___NODE = wpEntitiesRef['wordpress__wp_ac_ebook'][e.acf._ac_ebook_banner]
        delete e.acf._ac_ebook_banner
      }

      if (e.acf.hasOwnProperty('_ac_recommendations')) {
        e.ac_recommendations___NODE = []
        Array.isArray(e.acf._ac_recommendations) && e.acf._ac_recommendations.forEach(wpObj => {
          findWpEntityRef(wpObj.wordpress_id, ['wordpress__POST', 'wordpress__wp_ac_ebook', 'wordpress__wp_ac_media'], (ref) => {
            e.ac_recommendations___NODE.push(ref)
          }, `${e.type}_${e.wordpress_id}`)
        })

        delete e.acf._ac_recommendations
      }

      if (Array.isArray(e.acf._ac_ebook_previews)) {
        e.ac_ebook_previews = e.acf._ac_ebook_previews
        delete e.acf._ac_ebook_previews
      }
    }

    if (e.content) {
      e.readingTime = readingTime(e.content, {
        wordsPerMinute: 180
      });
    }

    if (Array.isArray(e.ac_author)) {
      e.ac_authors___NODE = e.ac_author.map(aId => ac_authors[aId])
      delete e.ac_author
    }

    // Replace options id with nodes
    if (e.__type === `wordpress__wp_options`) {

      // Link image fields
      Array('ac_logo', 'ac_logo_small', 'ac_favicon').forEach((key) => {
        console.log(e[key])
        findWpEntityRef(e[key], ['wordpress__wp_media'], (ref) => {
          console.log(ref)
          e[`${key}___NODE`] = ref
        }, `${e.type}_${e.wordpress_id}`)
        delete e[key]
      })

      Array('ac_blog_page', 'ac_essentials_page', 'ac_topics_page', 'ac_about_page', 'ac_contact_page', 'ac_questions_page').forEach((key) => {
        findWpEntityRef(e[key], ['wordpress__PAGE'], (ref) => {
          e[`${key}___NODE`] = ref
        }, `${e.type}_${e.wordpress_id}`)
        delete e[key]
      })


      Array('ac_ebook_banner').forEach((key) => {
        findWpEntityRef(e[key], ['wordpress__wp_ac_ebook'], (ref) => {
          e[`${key}___NODE`] = ref
        }, `${e.type}_${e.wordpress_id}`)
        delete e[key]
      })

      Array('ac_hero_slot_1', 'ac_hero_slot_2', 'ac_hero_slot_3', 'ac_hero_slot_4').forEach((key) => {
        e[key] && findWpEntityRef(e[key], ['wordpress__wp_ac_banner'], (ref) => {
          e[`${key}___NODE`] = ref
        }, `${e.type}_${e.wordpress_id}`)
        delete e[key]
      })

      Array('ac_banner_slot_1', 'ac_banner_slot_2', 'ac_banner_slot_3', 'ac_banner_slot_4').forEach((key) => {
        e[key] && findWpEntityRef(e[key], ['wordpress__wp_ac_banner'], (ref) => {
          e[`${key}___NODE`] = ref
        }, `${e.type}_${e.wordpress_id}`)
        delete e[key]
      })

      e.ac_popular_posts___NODE = []
      if (wpEntitiesRef['wordpress__POST'] && Array.isArray(e.ac_popular_posts)) {
        e.ac_popular_posts.forEach(pId => {
          findWpEntityRef(pId, ['wordpress__POST'], (ref) => {
            e.ac_popular_posts___NODE.push(ref)
          }, `${e.type}_${e.wordpress_id}`)
        })
        delete e.ac_popular_posts
      }

      e.ac_featured_posts___NODE = []
      if (wpEntitiesRef['wordpress__POST'] && Array.isArray(e.ac_featured_posts)) {
        e.ac_featured_posts.forEach(pId => {
          findWpEntityRef(pId, ['wordpress__POST'], (ref) => {
            e.ac_featured_posts___NODE.push(ref)
          }, `${e.type}_${e.wordpress_id}`)
        })
        delete e.ac_featured_posts
      }

      e.ac_featured_topics___NODE = []
      if (Array.isArray(e.ac_featured_topics)) {
        e.ac_featured_topics.forEach(tId => {
          findWpEntityRef(tId, ['wordpress__TAG'], (ref) => {
            e.ac_featured_topics___NODE.push(ref)
          }, `${e.type}_${e.wordpress_id}`)
        })
        delete e.ac_featured_topics
      }
    }

    return e
  })
} // TODO generalize this for all taxonomy types.

exports.mapPostsToTagsCategories = entities => {
  return entities.map(e => {
    // Replace tags & categories with links to their nodes.
    if (Array.isArray(e.tags)) {
      e.tags___NODE = []
      e.tags.forEach(tId => {
        findWpEntityRef(tId, ['wordpress__TAG'], (ref => e.tags___NODE.push(ref)), `${e.type}_${e.wordpress_id}`)
      })
      delete e.tags
    }

    let entityHasCategories = e.categories && Array.isArray(e.categories) && e.categories.length

    if (Array.isArray(e.categories)) {
      e.categories___NODE = []
      e.categories.forEach(cId => {
        findWpEntityRef(cId, ['wordpress__CATEGORY'], (ref => e.categories___NODE.push(ref)), `${e.type}_${e.wordpress_id}`)
      })
      e.categories___NODE = e.categories.map(c => wpEntitiesRef['wordpress__CATEGORY'][String(c)])
      delete e.categories
    }

    return e
  })
} // TODO generalize this for all taxonomy types.


exports.mapTagsCategoriesToTaxonomies = entities => entities.map(e => {
  // Where should api_menus stuff link to?
  if (e.taxonomy && e.__type !== `wordpress__wp_api_menus_menus`) {
    // Replace taxonomy with a link to the taxonomy node.
    const taxonomyNode = entities.find(t => t.wordpress_id === e.taxonomy);

    if (taxonomyNode) {
      e.taxonomy___NODE = taxonomyNode.id;
      delete e.taxonomy;
    }
  }

  return e;
});


exports.createAcImages = (createNodeId, entities) => {
  return entities;
  /*
  const AcImages = []
  const _imageIds = {}

  entities.forEach(e => {
    if (e.ac_image) {
      const wordpress_id = e.ac_image.id
      if (!_imageIds[wordpress_id]) {
        _imageIds[wordpress_id] = {
          wordpress_id: wordpress_id,
          id: createNodeId(`${'wordpress__wp_media'}-${wordpress_id.toString()}`),
          modified: e.ac_image.modified,
          __type: 'wordpress__wp_media'
        }
        AcImages.push(_imageIds[wordpress_id])
      }
    }
  })

  return Array.concat(entities, AcImages)*/
}


const h2p = require('html2plaintext')
exports.injectGlossary = (entities) => {
  const glossary = []
  const posts = []
  const words = {}

  entities.forEach(e => {
    if (e.__type === 'wordpress__wp_glossary') {
      glossary.push(e)
      words[e.title.toLowerCase()] = e
    }
    else if (e.__type === 'wordpress__POST' && e.content) posts.push(e)
    else if (e.__type === 'wordpress__wp_ac_essential' && e.content) posts.push(e)
  })

  if (!glossary.length) {
    console.log('No Glossary')
    return entities
  }

  let done = {}

  const replacer = (word) => {

    const origialWord=word
    word = word.toLowerCase()
    if (done[word]) return word

    const { content, slug } = words[word]
    let excerpt = h2p(content)

    if (excerpt.length > 235) {
      let parts = excerpt.substr(0, 220).split(' ')
      parts.pop()
      excerpt = parts.join(' ')
    }

    let url = `/glossary/${slug}`

    done[word] = `<span class="ac-glossary-link"><span class="glossary-tooltip"><span class="glossary-link"><a target="_blank" href="${url}">${origialWord}</a></span><span class="hidden glossary-tooltip-content clearfix"><span class="glossary-tooltip-text">${excerpt}<a target="_blank" href="${url}"> ... </a></span></span></span></span>`

    return done[word]
  }

  const regex = new RegExp(`(\\s)(${Object.keys(words).join('|')})([^a-zA-Z0-9]*)`, 'mi');

/*   const test = `Some random text containing ${glossary[0].title} should get replaced`

  if (test === test.replace(regex, replacer)) throw new Error('Replacer not working!')
 */
  posts.forEach(e => {
    done = {}
    const match = String(e.content).match(regex)
    if (match && match.length>2){
      e.content = String(e.content).replace(match[0], `${match[1]}${replacer(match[2])}${match[3]}`) 
    }
  });

  return entities
}


exports.mapElementsToParent = entities => entities.map(e => {
  if (e.wordpress_parent) {
    // Create parent_element with a link to the parent node of type.
    const parentElement = entities.find(t => t.wordpress_id === e.wordpress_parent && t.__type === e.__type);

    if (parentElement) {
      e.parent_element___NODE = parentElement.id;
    }
  }

  return e;
});

exports.mapPolylangTranslations = entities => entities.map(entity => {
  if (entity.polylang_translations) {
    entity.polylang_translations___NODE = entity.polylang_translations.map(translation => entities.find(t => t.wordpress_id === translation.wordpress_id && entity.__type === t.__type).id);
    delete entity.polylang_translations;
  }

  return entity;
});

exports.searchReplaceContentUrls = function ({
  entities,
  searchAndReplaceContentUrls
}) {
  if (!_.isPlainObject(searchAndReplaceContentUrls) || !_.has(searchAndReplaceContentUrls, `sourceUrl`) || !_.has(searchAndReplaceContentUrls, `replacementUrl`) || typeof searchAndReplaceContentUrls.sourceUrl !== `string` || typeof searchAndReplaceContentUrls.replacementUrl !== `string`) {
    return entities;
  }

  const sourceUrl = new RegExp(`https?:\/\/(${searchAndReplaceContentUrls.sourceUrl})\/?`, 'gi'),
        replacementUrl = searchAndReplaceContentUrls.replacementUrl;
  const _blacklist = [`_links`, `__type`, 'source_url','itunes_podcast_info'];

  const blacklistProperties = function blacklistProperties(obj = {}, blacklist = []) {
    for (var i = 0; i < blacklist.length; i++) {
      delete obj[blacklist[i]];
    }

    return obj;
  };

  return entities.map(function (entity) {
    const original = Object.assign({}, entity);
    const mediaCdnUrl="https://media.activechristianity.org"
    try {
      var whiteList = blacklistProperties(entity, _blacklist);
      var replaceable = JSON.stringify(whiteList);
      var replaced = replaceable.replace(sourceUrl, replacementUrl);
      var replaced = replaced.replace(/\/$/, "")
      var replaced = replaced.replace(new RegExp('\/wp-content\/uploads\/', 'g'), `${mediaCdnUrl}/`);
      var replaced = replaced.replace(new RegExp('http:', 'g'), 'https:');
      
      var parsed = JSON.parse(replaced);
    } catch (e) {
      console.log(colorized.out(e.message, colorized.color.Font.FgRed));
      return original;
    }

    return _.defaultsDeep(parsed, original);
  });
};

exports.mapEntitiesToMedia = entities => {
  const media = entities.filter(e => e.__type === `wordpress__wp_media`);
  return entities.map(e => {
    // Map featured_media to its media node
    // Check if it's value of ACF Image field, that has 'Return value' set to
    // 'Image Object' ( https://www.advancedcustomfields.com/resources/image/ )
    const isPhotoObject = field => _.isObject(field) && field.wordpress_id && field.url && field.width && field.height ? true : false;

    const isURL = value => _.isString(value) && value.startsWith(`http`);

    const isMediaUrlAlreadyProcessed = key => key == `source_url`;

    const isFeaturedMedia = (value, key) => (_.isNumber(value) || _.isBoolean(value)) && key === `featured_media`; // ACF Gallery and similarly shaped arrays

    const isArrayOfPhotoObject = field => _.isArray(field) && field.length > 0 && isPhotoObject(field[0]);

    const getMediaItemID = mediaItem => mediaItem ? mediaItem.id : null; // Try to get media node from value:
    //  - special case - check if key is featured_media and value is photo ID
    //  - check if value is media url
    //  - check if value is ACF Image Object
    //  - check if value is ACF Gallery


    const getMediaFromValue = (value, key) => {
      if (isFeaturedMedia(value, key)) {
        return {
          mediaNodeID: _.isNumber(value) ? getMediaItemID(media.find(m => m.wordpress_id === value)) : null,
          deleteField: true
        };
      } else if (isURL(value) && !isMediaUrlAlreadyProcessed(key)) {
        const mediaNodeID = getMediaItemID(media.find(m => m.source_url === value));
        return {
          mediaNodeID,
          deleteField: !!mediaNodeID
        };
      } else if (isPhotoObject(value)) {
        const mediaNodeID = getMediaItemID(media.find(m => m.source_url === value.url));
        return {
          mediaNodeID,
          deleteField: !!mediaNodeID
        };
      } else if (isArrayOfPhotoObject(value)) {
        return {
          mediaNodeID: value.map(item => getMediaFromValue(item, key).mediaNodeID).filter(id => id !== null),
          deleteField: true
        };
      }

      return {
        mediaNodeID: null,
        deleteField: false
      };
    };

    const replaceFieldsInObject = object => {
      let deletedAllFields = true;

      _.each(object, (value, key) => {
        const _getMediaFromValue = getMediaFromValue(value, key),
              mediaNodeID = _getMediaFromValue.mediaNodeID,
              deleteField = _getMediaFromValue.deleteField;

        if (mediaNodeID) {
          object[`${key}___NODE`] = mediaNodeID;
        }

        if (deleteField) {
          delete object[key]; // We found photo node (even if it has no image),
          // We can end processing this path

          return;
        } else {
          deletedAllFields = false;
        }

        if (_.isArray(value)) {
          value.forEach(v => replaceFieldsInObject(v));
        } else if (_.isObject(value)) {
          replaceFieldsInObject(value);
        }
      }); // Deleting fields and replacing them with links to different nodes
      // can cause build errors if object will have only linked properites:
      // https://github.com/gatsbyjs/gatsby/blob/master/packages/gatsby/src/schema/infer-graphql-input-fields.js#L205
      // Hacky workaround:
      // Adding dummy field with concrete value (not link) fixes build


      if (deletedAllFields && object && _.isObject(object)) {
        object[`dummy`] = true;
      }
    };

    replaceFieldsInObject(e);
    return e;
  });
}; // Downloads media files and removes "sizes" data as useless in Gatsby context.


exports.downloadMediaFiles =
/*#__PURE__*/
function () {
  var _ref = (0, _asyncToGenerator2.default)(function* ({
    entities,
    store,
    cache,
    createNode,
    createNodeId,
    touchNode,
    _auth
  }) {
    let i = 0
    return Promise.all(entities.map(
    /*#__PURE__*/
    function () {
      var _ref2 = (0, _asyncToGenerator2.default)(function* (e) {
        let fileNodeID;

        if (e.__type === `wordpress__wp_media`) {
          const mediaDataCacheKey = `wordpress-media-${e.wordpress_id}`;
          const cacheMediaData = yield cache.get(mediaDataCacheKey); // If we have cached media data and it wasn't modified, reuse
          // previously created file node to not try to redownload

          if (cacheMediaData && e.modified === cacheMediaData.modified) {
            fileNodeID = cacheMediaData.fileNodeID;
            touchNode({
              nodeId: cacheMediaData.fileNodeID
            });
          } // If we don't have cached data, download the file

          if (!fileNodeID) {
            try {
              const fileNode = yield createRemoteFileNode({
                url: e.source_url,
                store,
                cache,
                createNode,
                createNodeId,
                auth: _auth
              });

              if (fileNode) {
                fileNodeID = fileNode.id;
                yield cache.set(mediaDataCacheKey, {
                  fileNodeID,
                  modified: e.modified
                });
              }
            } catch (e) {// Ignore
            }
          }
        }

        if (fileNodeID) {
          e.localFile___NODE = fileNodeID;
          delete e.media_details.sizes;
        }

        return e;
      });

      return function (_x2) {
        return _ref2.apply(this, arguments);
      };
    }()));
  });

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();

const prepareACFChildNodes = (obj, entityId, topLevelIndex, type, children, childrenNodes) => {
  // Replace any child arrays with pointers to nodes
  _.each(obj, (value, key) => {
    if (_.isArray(value) && value[0] && value[0].acf_fc_layout) {
      obj[`${key}___NODE`] = value.map((v, indexItem) => prepareACFChildNodes(v, `${entityId}_${indexItem}`, topLevelIndex, type + key, children, childrenNodes).id);
      delete obj[key];
    }
  });

  const acfChildNode = Object.assign({}, obj, {
    id: entityId + topLevelIndex + type,
    parent: entityId,
    children: [],
    internal: {
      type,
      contentDigest: digest(JSON.stringify(obj))
    }
  });
  children.push(acfChildNode.id); // We recursively handle children nodes first, so we need
  // to make sure parent nodes will be before their children.
  // So let's use unshift to put nodes in the beginning.

  childrenNodes.unshift(acfChildNode);
  return acfChildNode;
};

exports.createNodesFromEntities = ({
  entities,
  createNode
}) => {
  entities.forEach(e => {
    // Create subnodes for ACF Flexible layouts
    let __type = e.__type,
        entity = (0, _objectWithoutPropertiesLoose2.default)(e, ["__type"]); // eslint-disable-line no-unused-vars

    let children = [];
    let childrenNodes = [];

    if (entity.acf) {
      _.each(entity.acf, (value, key) => {
        if (_.isArray(value) && value[0] && value[0].acf_fc_layout) {
          entity.acf[`${key}_${entity.type}___NODE`] = entity.acf[key].map((f, i) => {
            const type = `WordPressAcf_${f.acf_fc_layout}`;
            delete f.acf_fc_layout;
            const acfChildNode = prepareACFChildNodes(f, entity.id + i, key, type, children, childrenNodes);
            return acfChildNode.id;
          });
          delete entity.acf[key];
        }
      });
    }

    let node = Object.assign({}, entity, {
      children,
      parent: null,
      internal: {
        type: e.__type,
        contentDigest: digest(JSON.stringify(entity))
      }
    });
    createNode(node);
    childrenNodes.forEach(node => {
      createNode(node);
    });
  });
};
